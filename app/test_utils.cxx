#include <gtest/gtest.h>
#include <stdio.h>
#include <test.h>
#include "utils.h"

TEST(load_config, load_config_1) {
    ASSERT_EQ(1,1);
}
//    cpu_policy_t policies[3];
//    setting_t setting;
//    load_config(TEST_CONFIG_FILE, policies, &setting);
//    ASSERT_EQ(policies[0].max_freq, 1800000);
//}
//
//TEST(get_cpu_info, get_cpu_info_1) {
//    cpu_info_t cpu_info = get_cpu_info();
//    ASSERT_GT(cpu_info.max_freq, 1);
//}
//
//TEST(get_policy_based_on_loadavg, get_policy_based_on_loadavg_1) {
//    cpu_policy_t policies[3] = {
//        {
//            .loadavg_start = 0,
//            .loadavg_end = 1,
//            .online_procs = 1,
//            .max_freq = 1600000
//        },
//        {
//            .loadavg_start = 1,
//            .loadavg_end = 1.5,
//            .online_procs = 1,
//            .max_freq = 1700000
//        },
//        {
//            .loadavg_start = 2,
//            .loadavg_end = -1,
//            .online_procs = 1,
//            .max_freq = 1800000
//        }
//    };
//    size_t policy_size = sizeof(policies) / sizeof(policies[0]);
//    int idx;
//    cpu_policy_t policy1 = get_policy_based_on_loadavg(1, policies, policy_size, &idx);
//    cpu_policy_t policy1_5 = get_policy_based_on_loadavg(1.5, policies, policy_size, &idx);
//    cpu_policy_t policy2 = get_policy_based_on_loadavg(2, policies, policy_size, &idx);
//    cpu_policy_t policy3 = get_policy_based_on_loadavg(3, policies, policy_size, &idx);
//    ASSERT_EQ(policy1.max_freq, 1600000);
//    ASSERT_EQ(policy1_5.max_freq, 1700000);
//    ASSERT_EQ(policy2.max_freq, 1800000);
//    ASSERT_EQ(policy3.max_freq, 1800000);
//}
//
