#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include "utils.h"

#define DEFAULT_CONFIG_PATH "/etc/powersaver/config.yaml"
static char *config_file = DEFAULT_CONFIG_PATH;
int main(int argc, char **argv) {
    signal(SIGINT, reset_policy);
    signal(SIGTERM, reset_policy);
    signal(SIGSEGV, reset_policy);
    signal(SIGABRT, reset_policy);

    int opt;
    while((opt = getopt(argc, argv, "c:")) != -1) {
        switch(opt) {
            case 'c':
                config_file = optarg;
                break;
            case '?':
                exit(1);
        }
    }

    return runner(config_file);
}
