#ifdef __cplusplus
extern "C" {
#endif
#ifndef POWERSAVER_UTILS_H
#define POWERSAVER_UTILS_H

#include <sys/sysinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>

#define MAX_PROFILE_NUM 5
#define MAX_POLICY_NUM 10
#define MAX_KEY_LENGTH 30
#define MAX_VALUE_LENGTH 50

typedef struct cpu_info_t {
    int nprocs;
    int max_freq;
    int min_freq;
} cpu_info_t;

typedef struct cpu_limit_t {
    double loadavg_start;
    double loadavg_end;
    double max_freq;
    int online_procs;
} cpu_limit_t;

typedef enum {
    CPU,
    NET
} first_level_key;

typedef enum {
    CPU_PROFILE,
} third_level_key;

typedef enum {
    PROFILE,
    LOADAVG_UPDATE_RATE,
    LOADAVG_VARIANCE,
    POLICIES
} cpu_first_level_key;

typedef enum {
    LOADAVG_START,
    LOADAVG_END,
    ONLINE_PROCS,
    MAX_FREQ
} cpu_policy_first_level_key;

typedef enum {
    LISTEN_ADDR,
    LISTEN_PORT,
} net_first_level_key;

typedef struct cpu_profile_t {
    char profile[MAX_KEY_LENGTH];
    int loadavg_update_rate;
    double loadavg_variance;
    cpu_limit_t policies[MAX_POLICY_NUM];
    int policy_num;
} cpu_profile_t;

typedef struct net_t {
    char listen_addr[MAX_VALUE_LENGTH];
    char listen_port[MAX_VALUE_LENGTH];
} net_t;

typedef struct config_t {
    cpu_profile_t cpu_profiles[MAX_PROFILE_NUM];
    int cpu_profile_num;
    net_t net;
} config_t;

typedef struct app_context_t {
    config_t config;
    cpu_info_t cpu_info;
    int current_cpu_profile_idx;
    int current_cpu_policy_idx;
    int s_fd;
    pthread_t thread[2];
} app_context_t;

cpu_info_t get_cpu_info();
cpu_limit_t get_policy_based_on_loadavg(double loadavg, cpu_limit_t *policies, size_t policy_len, int *idx);
int limit_cpu_core(int max_cores, int online_procs, double max_freq);
void reset_policy();
double get_diff(double a, double b);
int runner(const char *config_file);


#endif //POWERSAVER_UTILS_H
#ifdef __cplusplus
}
#endif

