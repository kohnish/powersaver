#include "utils.h"
#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <pthread.h>
#include <resolv.h>
#include <sys/socket.h>
#include <time.h>
#include <yaml.h>

static app_context_t app_context;

int get_cpu_freq(const char *syspath, char *buf, size_t buf_size) {
    FILE *fp = fopen(syspath, "r");
    if (fp) {
        char *r_buf = fgets(buf, buf_size, fp);
        fclose(fp);
        if (r_buf != NULL) {
            return atoi(buf);
        } else {
            printf("WARN: Failed to get cpu frequency\n");
            return -1;
        }
    } else {
        printf("WARN: Failed to get cpu frequency\n");
        return -1;
    }
}

cpu_info_t get_cpu_info() {
    char buf[24] = {0};
    int max = get_cpu_freq("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq", buf, 24);
    int min = get_cpu_freq("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq", buf, 24);
    cpu_info_t cpu_info = {.max_freq = max, .min_freq = min, .nprocs = get_nprocs_conf()};
    return cpu_info;
}

int switch_core_status(char *syspath, int status) {
    if (geteuid() == 0) {
        FILE *fp = fopen(syspath, "r+");
        if (fp) {
            char buf[24] = {0};
            char *f_ret = fgets(buf, 24, fp);
            if (f_ret == NULL) {
                fclose(fp);
                return -1;
            }
            if (atoi(buf) == status) {
                fclose(fp);
                return 0;
            }
            fprintf(fp, "%i", status);
            fclose(fp);
        } else {
            printf("WARN: Failed to set cpu status to %i for %s\n", status, syspath);
        }
    } else {
        printf("WARN: Rootless, not setting cpu status to %i for %s\n", status, syspath);
    }
    return 0;
}

int set_cpu_freq(char *syspath, double max_freq) {
    if (geteuid() == 0) {
        FILE *fp = fopen(syspath, "r+");
        if (fp) {
            char buf[24] = {0};
            char *f_ret = fgets(buf, 24, fp);
            if (f_ret == NULL) {
                fclose(fp);
                return -1;
            }
            if (atoi(buf) == max_freq) {
                fclose(fp);
                return 0;
            } else {
                fprintf(fp, "%.0lf", max_freq);
                fclose(fp);
            }
        } else {
            printf("WARN: Failed to set cpu max_freq to %.0lf for %s\n", max_freq, syspath);
        }
    } else {
        printf("WARN: Rootless, not setting cpu max_freq to %.0lf for %s\n", max_freq, syspath);
    }
    return 1;
}

int limit_cpu_core(int max_cores, int online_procs, double max_freq) {
    int max_cores_from_zero = max_cores - 1;
    for (int i = max_cores_from_zero; i >= online_procs; i--) {
        char syspath[64] = {0};
        snprintf(syspath, 64, "/sys/devices/system/cpu/cpu%i/online", i);
        switch_core_status(syspath, 0);
    }
    for (int i = 1; i < online_procs; i++) {
        char syspath[64] = {0};
        snprintf(syspath, 64, "/sys/devices/system/cpu/cpu%i/online", i);
        switch_core_status(syspath, 1);
    }
    for (int i = 0; i < online_procs; i++) {
        char syspath[64] = {0};
        snprintf(syspath, 64, "/sys/devices/system/cpu/cpu%i/cpufreq/scaling_max_freq", i);
        set_cpu_freq(syspath, max_freq);
    }
    return 0;
}

cpu_limit_t get_policy_based_on_loadavg(double loadavg, cpu_limit_t *policies, size_t policy_len, int *idx) {
    size_t i = 0;
    for (i = 0; i <= policy_len; i++) {
        double loadavg_start = app_context.config.cpu_profiles[app_context.current_cpu_profile_idx].policies[i].loadavg_start;
        if (loadavg_start == 0 && i != 0) {
            loadavg_start = app_context.config.cpu_profiles[app_context.current_cpu_profile_idx].policies[i - 1].loadavg_end;
        }
        if (loadavg >= loadavg_start && (loadavg <= policies[i].loadavg_end || policies[i].loadavg_end <= 0 || i == policy_len)) {
            *idx = i;
            break;
        }
    }
    return policies[i];
}

void deinit() {
    if (app_context.s_fd) {
        close(app_context.s_fd);
    }
    if (app_context.thread[0]) {
        pthread_cancel(app_context.thread[0]);
        pthread_join(app_context.thread[0], NULL);
    }
    if (app_context.thread[1]) {
        pthread_cancel(app_context.thread[1]);
        pthread_join(app_context.thread[1], NULL);
    }
}

void reset_policy() {
    deinit();
    cpu_info_t cpu_info = get_cpu_info();
    for (int i = 0; i < cpu_info.nprocs; i++) {
        char syspath[64] = {0};
        if (i != 0) {
            snprintf(syspath, 64, "/sys/devices/system/cpu/cpu%i/online", i);
            switch_core_status(syspath, 1);
        }

        snprintf(syspath, 64, "/sys/devices/system/cpu/cpu%i/cpufreq/scaling_max_freq", i);
        set_cpu_freq(syspath, cpu_info.max_freq);
    }
    exit(0);
}

double get_diff(double a, double b) {
    if (a >= b) {
        return a - b;
    } else {
        return b - a;
    }
}

static void *loadavg_worker(void *worker_args) {
    (void)(worker_args);
    double last_loadavg = 0;
    double loadavg[1] = {0};
    int last_profile = -1;
    int last_policy = -1;

    while (getloadavg(loadavg, 1) != -1) {
        if (last_profile != app_context.current_cpu_profile_idx || get_diff(last_loadavg, loadavg[0]) >= app_context.config.cpu_profiles[app_context.current_cpu_profile_idx].loadavg_variance) {
            cpu_limit_t policy = get_policy_based_on_loadavg(loadavg[0], app_context.config.cpu_profiles[app_context.current_cpu_profile_idx].policies, app_context.config.cpu_profiles[app_context.current_cpu_profile_idx].policy_num, &(app_context.current_cpu_policy_idx));
            if (last_profile != app_context.current_cpu_profile_idx || last_policy != app_context.current_cpu_policy_idx) {
                printf("INFO: %lu: Updated the policy to index from %i to %i in %s due to loadavg change from %f to %f\n", (unsigned long)time(NULL), last_policy, app_context.current_cpu_policy_idx, app_context.config.cpu_profiles[app_context.current_cpu_profile_idx].profile, last_loadavg,
                       loadavg[0]);
                limit_cpu_core(app_context.cpu_info.nprocs, policy.online_procs, policy.max_freq);
                last_loadavg = loadavg[0];
                last_policy = app_context.current_cpu_policy_idx;
                last_profile = app_context.current_cpu_profile_idx;
            }
        }
        sleep(app_context.config.cpu_profiles[0].loadavg_update_rate);
    }
    pthread_exit(NULL);
}

static void rstrip(char *str, char c, size_t size) {
    for (size_t i = size; i != 0; i--) {
        if (str[i] == c) {
            str[i] = '\0';
            break;
        }
    }
}

static void *api_worker(void *worker_args) {
    (void)worker_args;
    app_context.s_fd = socket(AF_INET, SOCK_STREAM, 0);
    int reuse = 1;
    setsockopt(app_context.s_fd, SOL_SOCKET, SO_REUSEADDR, (const char *)&reuse, sizeof(reuse));
    setsockopt(app_context.s_fd, SOL_SOCKET, SO_REUSEPORT, (const char *)&reuse, sizeof(reuse));
    struct sockaddr_in s_addr;
    s_addr.sin_family = AF_INET;
    s_addr.sin_addr.s_addr = inet_addr(app_context.config.net.listen_addr);
    s_addr.sin_port = htons(atoi(app_context.config.net.listen_port));
    if (bind(app_context.s_fd, (struct sockaddr *)&s_addr, sizeof(s_addr)) < 0) {
        printf("ERROR: Bind failed \n");
        exit(1);
    }
    if (listen(app_context.s_fd, 1) < 0) {
        printf("ERROR: Listen failed \n");
        exit(1);
    }

    int running = 1;
    while (running) {
        struct sockaddr_in c_addr;
        int c_addr_len = sizeof(c_addr);
        int c_fd = accept(app_context.s_fd, (struct sockaddr *)&c_addr, (unsigned int *)&c_addr_len);
        char msg[1000] = {0};
        char recv_msg[100] = {0};
        int recv_ret = recv(c_fd, recv_msg, sizeof(recv_msg), 0);

        if (strcmp(recv_msg, "\n") == 0) {
            snprintf(msg, sizeof(msg), "Current profile is %s, online cores number is %d, max freq is %.0lf\n", app_context.config.cpu_profiles[app_context.current_cpu_profile_idx].profile,
                     app_context.config.cpu_profiles[app_context.current_cpu_profile_idx].policies[app_context.current_cpu_policy_idx].online_procs, app_context.config.cpu_profiles[app_context.current_cpu_profile_idx].policies[app_context.current_cpu_policy_idx].max_freq);
        } else {
            rstrip(recv_msg, '\n', sizeof(recv_msg));
            for (int i = 0; i < app_context.config.cpu_profile_num; i++) {
                if (strcmp(recv_msg, app_context.config.cpu_profiles[i].profile) == 0) {
                    // ToDo: make it atomic.
                    printf("INFO: Updating current profile to %s\n", recv_msg);
                    app_context.current_cpu_profile_idx = i;
                    snprintf(msg, sizeof(msg), "Set the profile to %s\n", app_context.config.cpu_profiles[i].profile);
                    break;
                } else {
                    printf("INFO: comparing %s and %s\n", recv_msg, app_context.config.cpu_profiles[i].profile);
                }
            }
        }

        if (recv_ret > 0) {
            ssize_t w_ret = write(c_fd, msg, sizeof(msg));
            if ((close(c_fd)) != 0 || w_ret < 0) {
                printf("ERROR: failed to write to the socket\n");
                running = 0;
            }
        }
    }
    pthread_exit(NULL);
}

config_t init_config(const char *config_file) {

    FILE *fp = fopen(config_file, "r");
    if (!fp) {
        printf("ERROR: Aborting, failed to open config file\n");
        abort();
    }

    config_t config;
    memset(&config, 0, sizeof(config));
    yaml_parser_t yp;
    yaml_parser_initialize(&yp);
    yaml_parser_set_input_file(&yp, fp);

    yaml_event_t ye;
    ye.type = YAML_STREAM_START_EVENT;

    int document_level = 0;
    int mapping_level = 0;
    int sequence_level = 0;
    int cpu_profile_counter = 0;
    int cpu_policy_counter = 0;
    int scalar_counter = 0;
    int counter[MAX_POLICY_NUM][MAX_POLICY_NUM] = {0};
    yaml_event_type_t last_event = YAML_STREAM_START_EVENT;
    char key_names[MAX_POLICY_NUM][MAX_POLICY_NUM][MAX_KEY_LENGTH] = {0};

    while (ye.type != YAML_STREAM_END_EVENT) {
        switch (ye.type) {
        case YAML_NO_EVENT: {
            printf("ERROR: Invalid yaml! Aborting the app.\n");
            abort();
            break;
        }
        case YAML_STREAM_START_EVENT: {
            last_event = YAML_STREAM_START_EVENT;
            break;
        }
        case YAML_DOCUMENT_START_EVENT: {
            last_event = YAML_DOCUMENT_START_EVENT;
            document_level++;
            break;
        }
        case YAML_DOCUMENT_END_EVENT: {
            last_event = YAML_DOCUMENT_END_EVENT;
            document_level--;
            break;
        }
        case YAML_MAPPING_START_EVENT: {
            last_event = YAML_MAPPING_START_EVENT;
            counter[sequence_level][mapping_level]++;
            mapping_level++;
            break;
        }
        case YAML_MAPPING_END_EVENT: {
            last_event = YAML_MAPPING_END_EVENT;
            counter[sequence_level][mapping_level]--;
            mapping_level--;
            break;
        }
        case YAML_SEQUENCE_START_EVENT: {
            last_event = YAML_SEQUENCE_START_EVENT;
            sequence_level++;
            break;
        }
        case YAML_SEQUENCE_END_EVENT: {
            last_event = YAML_SEQUENCE_END_EVENT;
            counter[sequence_level][mapping_level] = 0;
            sequence_level--;
            break;
        }
        case YAML_SCALAR_EVENT: {
            if (last_event == YAML_MAPPING_START_EVENT) {
                scalar_counter = 1;
            }
            scalar_counter++;

            if (last_event == YAML_MAPPING_START_EVENT || last_event == YAML_SEQUENCE_END_EVENT || (last_event == YAML_SCALAR_EVENT && (scalar_counter % 2) == 0)) {
                memset(key_names[sequence_level][mapping_level], '\0', MAX_POLICY_NUM);
                strncpy(key_names[sequence_level][mapping_level], (char *)ye.data.scalar.value, ye.data.scalar.length + 1);
                last_event = YAML_SCALAR_EVENT;
                break;
            }

            if (strcmp(key_names[0][1], "net") == 0) {
                if (strcmp(key_names[0][2], "listen_addr") == 0) {
                    strncpy(config.net.listen_addr, (char *)ye.data.scalar.value, ye.data.scalar.length + 1);
                } else if (strcmp(key_names[0][2], "listen_port") == 0) {
                    strncpy(config.net.listen_port, (char *)ye.data.scalar.value, ye.data.scalar.length + 1);
                } else {
                    printf("WARN: unknown data: %s\n", ye.data.scalar.value);
                }
            } else if (strcmp(key_names[0][1], "cpu") == 0) {
                cpu_profile_counter = counter[1][1] - 1;
                cpu_policy_counter = counter[2][2] - 1;
                if (strcmp(key_names[1][2], "profile") == 0) {
                    config.cpu_profile_num++;
                    strncpy(config.cpu_profiles[cpu_profile_counter].profile, (char *)ye.data.scalar.value, ye.data.scalar.length + 1);
                } else if (strcmp(key_names[1][2], "loadavg_update_rate") == 0) {
                    config.cpu_profiles[cpu_profile_counter].loadavg_update_rate = atoi((char *)ye.data.scalar.value);
                } else if (strcmp(key_names[1][2], "loadavg_variance") == 0) {
                    config.cpu_profiles[cpu_profile_counter].loadavg_variance = atof((char *)ye.data.scalar.value);
                } else if (strcmp(key_names[2][3], "loadavg_start") == 0) {
                    config.cpu_profiles[cpu_profile_counter].policies[cpu_policy_counter].loadavg_start = atof((char *)ye.data.scalar.value);
                } else if (strcmp(key_names[2][3], "loadavg_end") == 0) {
                    config.cpu_profiles[cpu_profile_counter].policies[cpu_policy_counter].loadavg_end = atof((char *)ye.data.scalar.value);
                } else if (strcmp(key_names[2][3], "online_procs") == 0) {
                    config.cpu_profiles[cpu_profile_counter].policies[cpu_policy_counter].online_procs = atoi((char *)ye.data.scalar.value);
                } else if (strcmp(key_names[2][3], "max_freq") == 0) {
                    (config.cpu_profiles[cpu_profile_counter].policy_num)++;
                    config.cpu_profiles[cpu_profile_counter].policies[cpu_policy_counter].max_freq = atof((char *)ye.data.scalar.value) * 1000;
                } else {
                    printf("WARN: unknown data: %s\n", ye.data.scalar.value);
                }
            } else {
                printf("WARN: unknown data: %s\n", ye.data.scalar.value);
            }
            break;
        }
        default: {
            printf("WARN: Unknown type: %i! Aborting the app\n", ye.type);
            abort();
            break;
        }
        }
        yaml_event_delete(&ye);
        yaml_parser_parse(&yp, &ye);
    }
    yaml_parser_delete(&yp);

    fclose(fp);

    return config;
}

void delay_start_on_boot() {
    struct sysinfo s_info;
    sysinfo(&s_info);
    if (s_info.uptime < 10) {
        printf("INFO: sleeping for 5 seconds on boot");
        sleep(5);
    }
}

int runner(const char *config_file) {
    delay_start_on_boot();
    app_context.config = init_config(config_file);
    app_context.cpu_info = get_cpu_info();

    pthread_create(&app_context.thread[0], NULL, &loadavg_worker, NULL);
    pthread_create(&app_context.thread[1], NULL, &api_worker, NULL);

    pause();

    return 0;
}

