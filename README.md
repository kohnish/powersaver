# Powersaver
Set a manual cpu profile based on load average.
Only works(tested) on intel machines.

## Features
  1. Specify multiple profile based on load average.
  2. Based on the profile, set CPU frequency for all online cores and online cores.

## Use cases
  1. Machines with limited use cases where maxmum cores / high cpu frequency are used even though it's unnecessary.
     e.g. A machine that just plays music, play films but sometimes used for compiling big applications.
  2. Laptops, just for the sake of prolonging battery life.

## Install options
  - Build and install it yourself. (For any linux distros and architectures)
  - Get fully statically linked binary from https://gitlab.com/kohnish/powersaver/-/releases (Only for x86-64)
  - dnf copr enable kohnish/main && dnf install powersaver (Not recommend as it is a copr repository for testing. For Fedora or similar only)

## Build
 - Look at pkg/powersaver.spec for reference

## Configuration
 - Look at misc/sample.json for reference

## Running
 - sudo powersaver -c sample.json or through systemd (misc/powersave.service)

## ToDo
 - Investigate abstracted API for CPU than /sys
 - Add an API for checking applied profile
