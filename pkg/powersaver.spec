Name:           powersaver
Version:        0.1
Release:        1%{?dist}
Summary:        Save power by limiting CPU power
License:        GPLv2
URL:            https://gitlab.com/kohnish/powersaver
Source0:        https://gitlab.com/kohnish/powersaver/-/archive/master/powersaver-master.tar.bz2
BuildRequires:  gcc make cmake libyaml-devel
Requires: libyaml

%description
Limit CPU based on specified load averages.

%prep
#%setup -q -n powersaver-%{version}
%setup -q -n powersaver-master

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=/usr ..
make %{?_smp_mflags}

%install
cd build
make install DESTDIR=%{buildroot}

%files
/etc/powersaver/sample.yaml
/usr/lib/systemd/system/powersaver.service
/usr/bin/powersaver

%changelog
* Mon May 04 2020 kohnish gmx <kohnish@gmx.com> - 1.5.1-1
- Initial

